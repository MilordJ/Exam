/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
        // Define changes to default configuration here:

        config.contentsCss = '/Exam/assets/css/fontas.css';

        //the next line add the new font to the combobox in CKEditor

        //config.font_names = '<Cutsom Font Name>/<YourFontName>;' + config.font_names;

        config.font_names = 'A3 Times AzLat/A3-Times-AzLat;' + config.font_names;
        config.toolbar = [

        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-'] },
        { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
//        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
//        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
	];
        config.height = '40px';
        config.width = '825px';
        
};
